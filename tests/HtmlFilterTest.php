<?php

namespace JontyNewman\Tests;

use PHPUnit\Framework\TestCase;
use JontyNewman\HtmlFilter;
use org\bovigo\vfs\vfsStream;

class HtmlFilterTest extends TestCase
{
	public function testFilter()
	{
		$name = __METHOD__;
		$root = vfsStream::setup();
		$in = "{$root->url()}/in";
		$input = '<input type="button" formaction="?\'=&">';
		$expected = '&lt;input type=&quot;button&quot; formaction=&quot;?&#039;=&amp;&quot;&gt;';
		$params = array('flags' => ENT_QUOTES);

		$this->assertTrue(HtmlFilter::register($name));
		$this->assertFalse(HtmlFilter::register($name));

		$this->assertSame(strlen($input), file_put_contents($in, $input));

		$handle = fopen($in, 'rb');

		$this->assertNotFalse($handle);

		$this->assertNotFalse(stream_filter_append($handle, $name, 0, $params));

		$this->assertSame($expected, stream_get_contents($handle));

		$this->assertTrue(fclose($handle));
	}

	public function testOpen()
	{
		$flags = ENT_QUOTES;
		$params = array('flags' => $flags);

		$handle = HtmlFilter::open(
			__FILE__,
			'rb',
			false,
			null,
			null,
			null,
			$params
		);

		$expected = $this->expected($flags);

		$this->assertSame($expected, stream_get_contents($handle));

		$this->assertTrue(fclose($handle));
	}

	public function testPassthru()
	{
		$name = __METHOD__;
		$flags = ENT_QUOTES;
		$params = array('flags' => $flags);

		ob_start();
		HtmlFilter::passthru(
			__FILE__,
			'rb',
			false,
			null,
			$name,
			null,
			$params
		);
		$actual = ob_get_clean();

		$this->assertSame($this->expected($flags), $actual);
	}

	private function expected($flags)
	{
		$contents = file_get_contents(__FILE__);

		$this->assertNotFalse($contents);

		return htmlspecialchars($contents, $flags);
	}
}
