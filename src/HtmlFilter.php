<?php

namespace JontyNewman;

use php_user_filter;
use RuntimeException;

/**
 * A stream filter for encoding special HTML characters.
 */
class HtmlFilter extends php_user_filter
{
	/**
	 * The default filter name.
	 */
	const NAME = __CLASS__;

	/**
	 * Registers the filter using the given name (or the default name if none is
	 * specified).
	 *
	 * @param string $name The name to use during registration (or NULL to use
	 * the default).
	 * @return bool Whether or not stream filter registration was successful.
	 */
	public static function register($name = null)
	{
		if (is_null($name)) {
			$name = self::NAME;
		}

		return stream_filter_register($name, __CLASS__);
	}

	/**
	 * Opens the specified file or URL with a HTML filter appended.
	 *
	 * @param string $filename The identifier (filename or URL) of the stream to
	 * open.
	 * @param string $mode The type of access required for the stream.
	 * @param bool $use_include_path Whether or not the include path should be
	 * searched.
	 * @param resource $context The context to associate with the stream.
	 * @param string $name The name of the filter to register and append.
	 * @param int $read_write The filter chain to attach the filter to.
	 * @param mixed $params The parameters to associate with the instance of the
	 * filter.
	 * @return resource The opened stream.
	 * @throws \RuntimeException The stream cannot be opened.
	 */
	public static function open(
		$filename,
		$mode,
		$use_include_path = false,
		$context = null,
		$name = null,
		$read_write = null,
		$params = null
	) {
		self::register($name);

		$handle = is_null($context)
			? fopen($filename, $mode, $use_include_path)
			: fopen($filename, $mode, $use_include_path, $context);

		if (false === $handle) {
			throw new RuntimeException('Failed to open stream');
		}

		if (is_null($name)) {
			$name = self::NAME;
		}

		$filter = stream_filter_append($handle, $name, $read_write, $params);

		if (false === $filter) {
			throw new RuntimeException('Failed to append filter');
		}

		return $handle;
	}


	/**
	 * Passes through the specified file or URL with a HTML filter appended.
	 *
	 * @param string $filename The identifier (filename or URL) of the stream to
	 * pass through.
	 * @param string $mode The type of access required for the stream.
	 * @param bool $use_include_path Whether or not the include path should be
	 * searched.
	 * @param resource $context The context to associate with the stream.
	 * @param string $name The name of the filter to register and append.
	 * @param int $read_write The filter chain to attach the filter to.
	 * @param mixed $params The parameters to associate with the instance of the
	 * filter.
	 * @throws \RuntimeException The stream cannot be passed through.
	 */
	public static function passthru(
		$filename,
		$mode,
		$use_include_path = false,
		$context = null,
		$name = null,
		$read_write = null,
		$params = null
	) {
		$handle = self::open(
			$filename,
			$mode,
			$use_include_path,
			$context,
			$name,
			$read_write,
			$params
		);

		if (false === fpassthru($handle)) {
			throw new RuntimeException('Failed to pass through stream');
		}

		if (false === fclose($handle)) {
			throw new RuntimeException('Failed to close stream');
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function filter($in, $out, &$consumed, $closing)
	{
		while ($bucket = stream_bucket_make_writeable($in)) {
			$bucket->data = $this->htmlspecialchars($bucket->data);
			$consumed += $bucket->datalen;
			stream_bucket_append($out, $bucket);
		}

		return PSFS_PASS_ON;
	}

	/**
	 * Converts special characters to HTML entities.
	 *
	 * @param string $string The string to convert.
	 * @return string The converted string.
	 */
	private function htmlspecialchars($string)
	{
		$flags = $this->param('flags', ENT_COMPAT | ENT_HTML401);
		$encoding = $this->param('encoding');
		$double_encode = $this->param('double_encode', true);

		if (is_null($encoding)) {
			$encoding = ini_get('default_charset');
		}

		return htmlspecialchars($string, $flags, $encoding, $double_encode);

	}

	/**
	 * Extracts the parameter associated with the given key (if any).
	 *
	 * @param scalar $key The key of the parameter to extract.
	 * @param mixed $default The default value of the parameter if it is not
	 * set.
	 * @return mixed The value of the parameter.
	 */
	private function param($key, $default = null)
	{
		if (isset($this->params[$key])) {
			$default = $this->params[$key];
		}

		return $default;
	}
}
