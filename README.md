# JontyNewman\HtmlFilter

A stream filter for converting special HTML characters.

## Installation

```
composer require 'jontynewman/html-filter ^1.0'
```

## Example

```php
<?php

use JontyNewman\HtmlFilter;

require 'vendor/autoload.php';

HtmlFilter::passthru(__FILE__, 'rb');

```

The static "passthru" method provides a shorthand for the following code.

```php
<?php

use JontyNewman\HtmlFilter;

require 'vendor/autoload.php';

$handle = HtmlFilter::open(__FILE__, 'rb');

if (false === fpassthru($handle)) {
    throw new RuntimeException('Failed to pass through stream');
}

if (false === fclose($handle)) {
    throw new RuntimeException('Failed to close stream');
}

```

In addition, the static "open" method provides a shorthand for the following
code.

```php
<?php

use JontyNewman\HtmlFilter;

require 'vendor/autoload.php';

HtmlFilter::register();

$handle = fopen(__FILE__, 'rb');

if (false === $handle) {
    throw new RuntimeException('Failed to open stream');
}

if (false === stream_filter_append($handle, HtmlFilter::NAME)) {
    throw new RuntimeException('Failed to append filter');
}

if (false === fpassthru($handle)) {
    throw new RuntimeException('Failed to pass through stream');
}

if (false === fclose($handle)) {
    throw new RuntimeException('Failed to close stream');
}

```